import os,sys,subprocess
project_list = []
for i in range(1,len(sys.argv) - 3):
  project_list.append(sys.argv[i].strip('[],'))
arguments_count = len(sys.argv) - 1
projects = sys.argv[1].strip('[]')
print("Project list is ",project_list)
environ = sys.argv[arguments_count - 2]
user = sys.argv[arguments_count - 1]
password = sys.argv[arguments_count]
for project in project_list:
  print("Deploying Project-->",project)
  os.getcwd()
  os.chdir(project)
  os.system("mvn -X -f pom.xml package deploy  -Dusername=%s -Dpassword=%s -Denvironment=%s -DmuleDeploy" %(user,password,environ))
  os.chdir('..')